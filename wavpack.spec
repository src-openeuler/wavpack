Name:           wavpack
Version:        5.8.0
Release:        1
Summary:        Hybrid Lossless Wavefile Compressor
License:        BSD-3-Clause
Url:            http://www.wavpack.com/
Source:         https://github.com/dbry/WavPack/releases/download/%{version}/%{name}-%{version}.tar.xz

BuildRequires:  autoconf automake libtool
BuildRequires:  gettext-devel

Recommends:     %{name}-help = %{version}-%{release}

%description
WavPack is a completely open audio compression format providing lossless, high-quality
lossy, and a unique hybrid compression mode. For version 5.0.0, several new file formats
and lossless DSD audio compression were added, making WavPack a universal audio archiving
solution.

%package devel
Summary:   Development files for wavpack
Requires:  %{name} = %{version}-%{release} pkgconfig

%description devel
The package contains libraries and header files for developing applications that use
wavpack.

%package help
Summary:   Help document for the wavpack package
Buildarch: noarch

%description help
Help document for the wavpack package.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -ivf
%configure --disable-static --disable-rpath

%make_build

%install
%make_install
%delete_la

%files
%{_docdir}/*
%license COPYING
%{_bindir}/*
%{_libdir}/libwavpack.so.*

%files devel
%doc ChangeLog doc/*.pdf
%{_includedir}/*
%{_libdir}/pkgconfig/*
%{_libdir}/*.so

%files help
%{_mandir}/man1/*.1*

%changelog
* Tue Jan 28 2025 Funda Wang <fundawang@yeah.net> - 5.8.0-1
- update to 5.8.0

* Fri Mar 22 2024 maqi <maqi@uniontech.com> - 5.7.0-1
- Upgrade to 5.7.0

* Tue May 09 2023 xu_ping <707078654@qq.com> - 5.6.0-1
- Upgrade to 5.6.0

* Tue May 10 2022 caodongxia <caodongxia@h-partners.com> - 5.3.0-3
- License compliance rectification

* Thu Mar 4 2021 wangxiao <wangxiao65@huawei.com> - 5.3.0-2
- Fix CVE-2020-35738

* Sat Nov 28 2020 lingsheng <lingsheng@huawei.com> - 5.3.0-1
- Update to 5.3.0
- Fix OSS-Fuzz issue 19925 19928 20060 20448

* Sat May 14 2020 lutianxiong <lutianxiong@huawei.com> - 5.1.0-12
- Type:bugfix
- ID:NA
- SUG:NA
- Fix memory leak on opening corrupted files 

* Sat Mar 14 2020 wangzhishun <wangzhishun1@huawei.com> - 5.1.0-11
- Make sure sample rate is specified and non-zero in DFF files
- Fix potential out-of-bounds heap read 

* Fri Oct 25 2019 Lijin Yang <yanglijin@huawei.com> - 5.1.0-10
- Package init
